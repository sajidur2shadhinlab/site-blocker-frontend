import React from 'react'
import {createRoot} from 'react-dom/client'
import Blocker from '../components/Blocker/Blocker'
import './popup.css'

const Popup = ()=>{
    return (
        <div>
            {/* <Header /> */}
            {/* <Profile /> */}
            <Blocker />
            {/* <AccessLog /> */}
            <br />
            <br />
            <br />
        </div>
    )
}

export {Popup}


const container = document.createElement('div')
document.body.appendChild(container)
const root = createRoot(container)
root.render(<Popup/>)