import React from "react"
import './Profile.css'
import Img from '../../static/icon.png'

const Profile = ()=>{
    return (
        <div className='Profile'>
            <img src={Img} alt="img" />
        </div>
    )
}

export default Profile