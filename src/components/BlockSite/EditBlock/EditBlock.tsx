import React, { useState, useEffect } from 'react'
import './EditBlock.css'

const EditBlock = (props)=>{
    const [edit, setEdit] = useState({title: '', site_url: '', redirect_url:'', message:''})

    useEffect(()=>{
        setEdit(props.data)
    },[])


    const addHttpAndWww=(url)=> {
        if (!/^https?:\/\//i.test(url)) {
          url = "http://" + url;
        }
        if (!/^www\./i.test(url)) {
          url = url.replace(/^(https?:\/\/)?/i, "$1www.");
        }
        return url;
      }

    const submit = (e)=>{
        e.preventDefault()


        chrome.storage.local.get('blocksite', (result)=>{
            let arr = result.blocksite || []
            // arr[props.id] = {...edit, site_url: addHttpAndWww(edit.site_url), redirect_url: addHttpAndWww(edit.redirect_url)}
            arr[props.id] = edit
            chrome.storage.local.set({'blocksite':arr})
        })

        props.click(true)
        props.setEdit(false)
    }

    return (
        <div className='EditBlock'>
            <input type="text" placeholder='Title' value={edit.title} onChange={e=>setEdit({...edit, title: e.target.value})}/>
            <input type="text" placeholder='Site url' value={edit.site_url} onChange={e=>setEdit({...edit, site_url: e.target.value})}/>
            <input type="text" placeholder='Redirect url' value={edit.redirect_url} onChange={e=>setEdit({...edit, redirect_url: e.target.value})}/>
            <input type="text" placeholder='Message' value={edit.message} onChange={e=>setEdit({...edit, message: e.target.value})}/>
            <button onClick={e=>submit(e)}>Update</button>
        </div>
    )
}

export default EditBlock