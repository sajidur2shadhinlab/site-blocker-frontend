import React, {useState, useEffect} from 'react'
import './BlockSite.css'
import EditBlock from './EditBlock/EditBlock'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'

const BlockSite = ()=>{
    const [site, setSite] = useState({title: '', site_url: '', redirect_url:'', message:''})
    const [allSites, setAllSites] = useState([])
    const [isSubmit, setIsSubmit] = useState(false)
    const [isEdit, setIsEdit] = useState(null)

    const submit = (e)=>{
        e.preventDefault()
        chrome.storage.local.get('blocksite', (result)=>{
            let arr = result.blocksite || []
            arr.push({...site, site_url:httpAndWww(site.site_url), redirect_url: httpAndWww(site.redirect_url)                    })
            chrome.storage.local.set({'blocksite':arr})
            setIsSubmit(true)
            setSite({title: '', site_url: '', redirect_url:'', message:''})
        })
    }


    const removeAll = (e)=>{
        e.preventDefault()
        chrome.storage.local.set({'blocksite':[]})
        setIsSubmit(true)
    }


    const edit = (e, key)=>{
        e.preventDefault()
        if(isEdit !== null){
            setIsEdit(null)
        }
        else{
            setIsEdit(key)
        }

    }

    const remove = (e,key)=>{
        e.preventDefault()

        chrome.storage.local.get('blocksite', (result)=>{
            let arr = result.blocksite || []
            arr.splice(key, 1)
            chrome.storage.local.set({'blocksite':arr})
        })
        setIsSubmit(true)
    }

    const clearField = (e)=>{
        e.preventDefault()
        setSite({title: '', site_url: '', redirect_url:'', message:''})
    }


    useEffect(()=>{
        setIsSubmit(false)
        chrome.storage.local.get('blocksite', (result)=>{
            setAllSites(result.blocksite || [])
        })          
    },[isSubmit])


    const httpAndWww=(url)=>{
        if(url.slice(0,4) !== 'http'){
            url = 'http://'+url
        }
        if(url.split('//')[1].slice(0,3) !== 'www'){
            let f = url.split('//')[0] 
            let l = url.split('//')[1]

            url = f + '//' + 'www.'+ l
        }
        return url
    }
      

    return (
        <div className='BlockSite'>
            <div className="head">
                <h1>Block Site</h1>
                <button onClick={(e)=>removeAll(e)}>All delete</button>
            </div>

            <div className='BlockForm'>
                <input type="text" placeholder='Title' onChange={(e)=>setSite({...site, title: e.target.value})} value={site.title}/>
                <div>
                    <input type="text" placeholder='Site url' onChange={(e)=>setSite({...site, site_url: e.target.value})} value={site.site_url}/>
                    <input type="text" placeholder='Redirect url' onChange={(e)=>setSite({...site, redirect_url: e.target.value})} value={site.redirect_url}/>
                </div>
                <textarea placeholder='Message'  rows={3} onChange={(e)=>setSite({...site, message: e.target.value})} value={site.message}></textarea>
                <button onClick={(e)=>submit(e)}>Submit</button><span> Or <p onClick={(e)=>clearField(e)}>Cancel</p></span>
            </div>

            <div className='SiteList'>
                    <div>
                        <div>
                            <p><b>Title</b></p>
                            <p><b>Site url</b></p>
                            <p><b>Redirect url</b></p>
                            <p><b>Message</b></p>
                        </div>
                    </div>
                {allSites.map((v,i)=>{
                    return (
                        <div key={i}>
                            <div>
                                <div><p>{v.title}</p></div>
                                <div><p>{v.site_url}</p></div>
                                <div><p>{v.redirect_url}</p></div>
                                <div><p>{v.message}</p></div>
                                <div className='SiteListEdit'>                 
                                    <button onClick={e=>edit(e, i)}><FontAwesomeIcon icon={faEdit} /></button>
                                    <button onClick={(e)=>remove(e, i)}><FontAwesomeIcon icon={faTrash} /></button>
                                </div>
                            </div>

                            {isEdit === i? <EditBlock data={v} id={i} click={setIsSubmit} setEdit={setIsEdit}/>:null}
                        </div>)
                })}
            </div>

        </div>
    )
}

export default BlockSite