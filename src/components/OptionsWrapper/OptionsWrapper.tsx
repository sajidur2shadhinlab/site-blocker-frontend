import React, {useState} from 'react'
import './OptionsWrapper.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faShieldAlt, faChartSimple, faGear} from '@fortawesome/free-solid-svg-icons'
import {BlockSite, AccessLog} from '../index'


const OptionsWrapper = ()=>{
    const [option, setOption] = useState('block-site')
    return (
        <div className='OptionsWrapper'>
            <div className='SideBar'>
                <p onClick={()=>setOption('block-site')}><FontAwesomeIcon icon={faShieldAlt} /> Block site</p>
                <p onClick={()=>setOption('logs')}><FontAwesomeIcon icon={faChartSimple} /> Access log</p>
            </div>

            <div>
                {option === 'block-site'? <BlockSite />:null}
                {option === 'logs'? <AccessLog /> :null}
            </div>
        </div>
    )
}

export default OptionsWrapper