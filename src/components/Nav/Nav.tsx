import React from 'react'
import './Nav.css'
import Icon from '../../static/icon.png'

const Nav = ()=>{
    return (
        <div className='Nav'>
             <div className="logo">
                <img src={Icon} alt="icon" />
                <h3>Shadhin Site Blocker</h3>
             </div>
        </div>
    )
}


export default Nav