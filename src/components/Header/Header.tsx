import React from "react"
import './Header.css'
import {Nav} from '../index'

const Header = ()=>{
    return (
        <div  className="Header">
            <Nav />
        </div>
    )
}

export default Header