import React, {useEffect, useState} from 'react'
import './BlockList.css'

const BlockList = ()=>{
    const [add, setAdd] = useState(false)
    const [site, setSite] = useState({title:'', site_url:'', redirect_url:'', custom_message: '', block_type:'block'})
    const [allSites, setAllSites] = useState([])


    const submit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>)=>{
        e.preventDefault()
        chrome.storage.local.get('sitelist', (result)=>{
            let arr = result.sitelist || []
            arr.push(site)
            chrome.storage.local.set({'sitelist':arr})
            setSite({title:'', site_url:'', redirect_url:'', custom_message: '', block_type:'block'})
        })

    }


    const allDelete = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>)=>{
        e.preventDefault()
        chrome.storage.local.set({'sitelist':[]})
    }


    useEffect(()=>{
        chrome.storage.local.get('sitelist', (result)=>{
            setAllSites(result.sitelist || [])
        })
    },[submit, allDelete])


    console.log(allSites)
    
    return (
        <div className='BlockList'>
            <h2>Block site list</h2>
            <div className="blocklist-btn">
                <button onClick={()=>setAdd(!add)}>{add?'Close':'Add site'}</button>
                <button onClick={(e)=>allDelete(e)}>All Delete</button>
            </div>
            {
                add ?
                <div className='addForm'>
                    <input type="text" placeholder='Title' value={site.title} onChange={(e)=>setSite({...site, title: e.target.value})}/>
                    <input type="text" placeholder='Site URL' value={site.site_url} onChange={(e)=>setSite({...site, site_url: e.target.value})}/>
                    <input type="text" placeholder='Redirect URL' value={site.redirect_url} onChange={(e)=>setSite({...site, redirect_url: e.target.value})}/>
                    <input type="text" placeholder='Message' value={site.custom_message} onChange={(e)=>setSite({...site, custom_message: e.target.value})}/>
                    <button onClick={(e)=>submit(e)}>Submit</button>
                </div>
                :null
            }

            
            <div className="block-list">
                {allSites.map((v,i)=>{
                    return (<div key={i}>
                        <p>{v.title}</p>
                        <p>{v.site_url}</p>
                    </div>)
                })}
            </div>

        </div>
    )
}

export default BlockList