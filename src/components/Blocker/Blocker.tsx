import React, {useState} from 'react'
import BlockList from './BlockList/BlockList'
import './Blocker.css'

const Blocker = ()=>{
    const [blockMode, setBlockMode] = useState('block')

    return (
        <div className='Blocker'>
            <button className={blockMode==='block'? 'active':'deactive'} onClick={()=>setBlockMode('block')}>Bock mode</button>
            <button className={blockMode==='enable'? 'active':'deactive'} onClick={()=>setBlockMode('enable')}>Enable mode</button>

            {blockMode==='block'?<BlockList />:null}
        </div>
        )
}

export default Blocker