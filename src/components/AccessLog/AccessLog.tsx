import React, {useState, useEffect} from 'react'
import './AccessLog.css'

const AccessLog = ()=>{
    const [log, setLog] = useState([])

    useEffect(()=>{
        chrome.storage.local.get('accesslog', (result)=>{
            setLog(result.accesslog || [])
        })          
    },[])

    console.log(log)
    return (
        <div className='AccessLog'>
            <div className="log-head">
                <h1>Access log</h1>
                <div className='log'>
                    <div>
                        
                            <p><b>Title</b></p>
                            <p><b>Site url</b></p>
                            <p><b>Redirect url</b></p>
                            <p><b>Message</b></p>
                            <p><b>Block time</b></p>
                        
                    </div>
                {
                    log.map((v, i)=>{
                        return (
                            <div key={i}>
                                <div>
                                    <p>{v.title}</p>
                                </div>
                                <div>
                                    <p>{v.site_url}</p>
                                </div>
                                <div>
                                    <p>{v.redirect_url}</p>
                                </div>
                                <div>
                                    <p>{v.message}</p>
                                </div>
                                <div>
                                    <p>{v.block_time}</p>
                                </div>
                            </div>
                        )
                    })
                }
                </div>
            </div>
        </div>
        )
}


export default AccessLog