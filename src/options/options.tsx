import React from 'react'
import {createRoot} from 'react-dom/client'
import './options.css'
import {Header, OptionsWrapper} from '../components'

const Options = ()=>{
    return (
        <div className='Options'>
            <Header />
            <OptionsWrapper />
        </div>
    )
}


const container = document.createElement('div')
document.body.appendChild(container)
const root = createRoot(container)
root.render(<Options />)