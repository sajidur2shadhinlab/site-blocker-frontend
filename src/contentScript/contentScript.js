const generateHTML = (redirect, custom_message) => {
    return `
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Blocked..!!!</title>
            <style>
                :root{
                    --white: #ffffff;
                    --red: #f15048;
                    --grey: #f3f3f3;
                    --black: #434343;
                }
                html,body{
                    position: relative;
                    width: 100%;
                    height: 100%;
                    margin: 0;
                    padding: 0;
                    font-family: Arial, Helvetica, sans-serif;
                }
                .half{
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                    height: 50%;
                    background-color: var(--red);
                }
                .box{
                    position: absolute;
                    width: 40%;
                    height: 40%;
                    background-color: var(--grey);
                    border-radius: 10px;
                    top: 30%;
                    left: 30%;
                    z-index: 100;
                }
                .middle{
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    width: 100%;
                }
                h1{
                    color: var(--red);
                    font-size: 40px;
                    text-transform: uppercase;
                    text-align: center;
                    margin: 0;
                }
                h2{
                    color: var(--black);
                    padding: 20px;
                    font-weight: 500;
                    text-align: center;
                    margin: 0;
                }
                h3{
                    color: var(--white);
                    background: var(--red);
                    display: inline-block;
                    padding: 10px 20px;
                    border-radius: 4px;
                    text-align: center;
                    position: absolute;
                    left: 50%;
                    transform: translate(-50%, 0);
                }
                a {
                    color: var(--white);
                    opacity: 0.8;
                    text-decoration: none;
                }
            </style>
        </head>
        <body>
            <div class="box">
                <div class="middle">
                    <h1>Blocked..!</h1>
                    <h2>${custom_message.length !== 0 ? custom_message : 'This site has been blocked by user.'}</h2>
                    <h3><a href='${redirect}'>Redirect</a></h3>
                </div>
            </div>
            <div class="half"></div>
        </body>
    </html>

    `;
};

const currentTime = () => {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    let dateTime = date + ' ' + time;
    return dateTime;
};

chrome.storage.local.get(['blocksite']).then((result) => {
    for (let v = 0; v < result.blocksite.length; v++) {
        if (
            'http://' + window.location.hostname == result.blocksite[v].site_url ||
            'https://' + window.location.hostname == result.blocksite[v].site_url
        ) {
            console.log(result.blocksite[v].site_url);
            //accesslog
            chrome.storage.local.get('accesslog', (log) => {
                let arr = log.accesslog || [];
                arr.push({
                    title: result.blocksite[v].title,
                    site_url: result.blocksite[v].site_url,
                    redirect_url: result.blocksite[v].redirect_url,
                    message: result.blocksite[v].message,
                    block_time: currentTime(),
                });
                chrome.storage.local.set({ accesslog: arr });
            });

            // html page
            document.write(
                generateHTML(
                    (redirect = result.blocksite[v].redirect_url),
                    (custom_message = result.blocksite[v].message)
                )
            );
            break;
        }
    }
});
